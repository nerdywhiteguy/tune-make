#!/bin/bash
if [ -z "$1" ] || [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
	cat << EOH
Helper script to generate algorithmic music.
Takes a snippet of C code and inserts it into a tiny C program
to generate interesting sounds.
The C snippet will be insterted into the following line:
	for(;;t++) { putchar( YOUR_CODE_HERE ) }

Written by NerdyWhiteGuy.

Parameters: [-o OUTPUT_FILE] [-t TITLE | -x] INPUT [ACTION] [ARGS]

-o OUTPUT_FILE:
	Controls the file to where the executable will be written.
	Defaults to "./music".

-t TITLE
	Print TITLE at the beginning of the program.
	(Printed to standard error so as not to interfere with audio output.)

-x
	INPUT contains the entire C program, not just a snippet.
	Don't forget to #include <stdio.h> and maybe <math.h>.
	But if you're using this option you should know that already. ;-)

INPUT may be a dash ("-"), indicating to read standard input.
INPUT may be a path beginning with a dot (".") or slash ("/"),
	meaning to read code from that file. To specify relative paths,
	use "./file" instead of just "file".
INPUT is considered C code, otherwise.

ACTION may be "make", "run", "play", "save", or "sox".
	make:
		Compile the C program into an executable.
	run:
		make and then run the executable, sending raw binary data to stdout.
	play (or aplay):
		make and run the executable, piping it's output to aplay.
		If no action is specified, play is assumed.
	save:
		make and run the executable, saving the first DURATION to FILE,
		where FILE and DURATION are the 1st and 2nd ARGS, respecively.
	sox:
		make and run the executable, piping it's output to sox.
		Note that you must specify a dash ("-") as the input file to sox.

	!!!WARNING!!! The ACTION run will output binary data, and a LOT of it.
	Be sure to either write or pipe this somewhere OTHER than your terminal!
	If you send it to your terminal, BAD THINGS WILL HAPPEN, including
	causing YOUR ENTIRE COMPUTER TO LOCK UP as the C program outputs as fast
	as it can and your terminal is frantically scrolling as fast as it can!
	Since there is so much, you stand a significant chance of encountering
	the byte 0x1b, which is the start of an ANSI escape sequence. These can
	set MANY properties, including things NOT EASILY RESETABLE.
	This program comes with ABSOLUTELY NO WARRANTY in ANY circumstances,
	but ESPECIALLY if you do THIS.
	YOU WILL HAVE A BAD DAY IF YOU TRY IT. YOU HAVE BEEN WARNED!
	
	Note that while action play/aplay does not require and addional options,
	both the save and sox options require numerous additional parameters.

ARGS:
	If ACTION is play or aplay,
		any additional arguments will be passed to aplay.
		See \"aplay --help\" for a list of it's args.
	If ACTION is save,
		the first element of ARGS must be the path of the FILE to save,
		and the second element must be the DURATION to save.
	If ACTION is save or sox,
		any additional arguments will be passed to sox.
		See \"sox --help\" for a list of it's args.
		Note that you MUST specify -t, -e, -b, and -r. For example:
		-t raw -e unsigned-integer -b 8 -r 8000
	If ACTION is sox,
		you must specify - for the input file, and it is recommended to use the
		trim filter to prevent files growing to arbitrary sizes.

Examples:
	./tune-make.sh "t*(42&t>>10)"
		Plays the "Fourty-Two Melody".
	./tune-make.sh ./file.txt play -r 48000
		Play the algorithm read from file.txt at a rate of 48000 Hz.
	./tune-make.sh - play -c 2
		Read algorithm from standard input and play it in stereo.
	./tune-make.sh "(t/0xf)>>(t*2&0xf0)" make
		Compile an executable and that's it.
	./tune-make.sh "(t/0xf)>>(t*(t>>12)&0xf0)" save "./Pulsing Scales.flac" 1:00 -t raw -e unsigned-integer -b 8 -c 2 -r 8000
		Save 1 minute of pulsing scales to a flac.
	./tune-make.sh "(t/0xf)>>(t*(t>>12)&0xf0)" sox -t raw -e unsigned-integer -b 8 -c 2 -r 8000 - "./Pulsing Scales.flac" trim 0 1:00
		Equivalent to the above.
	./tune-make.sh "t*200*(int)log(t%1000)*pow(2,t/1000-8)" sox -t raw -e unsigned-integer -b 8 -r 8000 - "Two Seconds of Fame.wav" trim 0 2
		Minimal sox example, showing all required arguments.
	./tune-make.sh "t*200*(int)log(t%1000)*pow(2,t/1000-8)" save "Two Seconds of Fame.wav" 2 -t raw -e unsigned-integer -b 8 -r 8000
		Equivalent using save action.
	./tune-make.sh "t*200*(int)log(t%1000)*pow(2,t/1000-8)" -d 2
		Equivalent to only play the sound

EOH
	exit
fi
OUTPUT=./music
PRE_CODE="#include <math.h>
#include <stdio.h>
int main(int t){"
MID_CODE="for(;;t++){putchar("
END_CODE=");}}"
if [[ $1 == "-o" && $2 != "" ]]; then OUTPUT=$2; shift 2; fi
if [[ $1 == "-x" ]]; then
	PRE_CODE=
	MID_CODE=
	END_CODE=
	shift 1
elif [[ $1 == "-t" ]]; then
	TITLE="${2//\\/\\\\}"
	TITLE="${TITLE//
/\\n}"
	TITLE="${TITLE//	/\\t}"
	TITLE="${TITLE//%/%%}"
	TITLE="${TITLE//\"/\\\"}"
	MID_CODE="fprintf(stderr,\"$TITLE\\n\");$MID_CODE"
	shift 2
fi
{
	echo -n "$PRE_CODE$MID_CODE"
	if [ "$1" == "-" ]; then
		cat
	elif [ "${1:0:1}" == "." ] || [ "${1:0:1}" == "/" ]; then
		cat "$1"
	else
		echo -n "$1"
	fi
	echo -n "$END_CODE"
} | tee "$OUTPUT.c" | gcc -w -o "$OUTPUT" -x c - -lm &&
case "$2" in
	make)
		# Do nothing because we're already done.
		;;
	run)
		"$OUTPUT"
		;;
	play|aplay)
		shift 2
		"$OUTPUT" | aplay "$@"
		;;
	save)
		FILE="$3"
		DURATION="$4"
		if [ -z "$FILE" ] || [ -z "$DURATION" ]; then
			echo "ERROR: Too few arguments."
			echo "When ACTION is save, ARGS must begin with FILE and DURATION."
			exit 2
		fi 1>&2
		echo "$@"
		shift 4
		echo "$@"
		echo FILE="$FILE"
		echo DURATION="$DURATION"
		"$OUTPUT" | sox "$@" - --comment "`cat $OUTPUT.c`" "$FILE" trim 0 "$DURATION"
		;;
	sox)
		shift 2
		"$OUTPUT" | sox "$@"
		;;
	*)
		shift 1
		"$OUTPUT" | aplay "$@"
		;;
esac
